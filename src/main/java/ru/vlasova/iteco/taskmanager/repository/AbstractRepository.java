package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IRepository;
import ru.vlasova.iteco.taskmanager.entity.AbstractEntity;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    private final Map<String, E> entities = new HashMap<>();

    @Override
    @Nullable
    public E findOne(@NotNull final String id) {
        return entities.get(id);
    }

    @Override
    @NotNull
    public E persist(@NotNull final E entity) throws DuplicateException {
        if (entities.containsValue(entity)) throw new DuplicateException("Such entity exists.");
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void merge(@NotNull final E entity) {
        entities.put(entity.getId(), entity);
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId) {
        @Nullable final List<E> entityList = new ArrayList<>();
        for(@NotNull E entity : entities.values()) {
            if(entity.getUserId() != null && entity.getUserId().equals(userId)) {
                entityList.add(entity);
            }
        }
        return entityList;
    }

    @Override
    @Nullable
    public E findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = entities.get(id);
        if (entity == null) return null;
        if(userId.equals(entity.getUserId())) return entity;
        return null;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = entities.get(id);
        if(userId.equals(entity != null ? entity.getUserId() : null)) {
            entities.remove(id);
        }
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @Nullable final List<E> entities = findAll(userId);
        for (@NotNull E entity : entities) {
            remove(userId, entity.getId());
        }
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    @NotNull
    public List<E> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public void remove(@NotNull final String id) {
        entities.remove(id);
    }

    @Override
    public int count() {
        return entities.size();
    }

}
