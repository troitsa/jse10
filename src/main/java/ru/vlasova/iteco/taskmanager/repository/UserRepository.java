package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.entity.User;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public String checkUser(@NotNull final String login) {
        @Nullable final List<User> users = findAll();
        for (@Nullable User user : users) {
            if(login.equals(user.getLogin())) return user.getId();
        }
        return null;
    }

}