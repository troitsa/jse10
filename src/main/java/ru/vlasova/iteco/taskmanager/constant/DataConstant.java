package ru.vlasova.iteco.taskmanager.constant;

import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class DataConstant {

    @NotNull public static final String DIR_SAVE = System.getProperty("user.dir") + File.separator + "dataload" + File.separator;
    @NotNull public static final String FILE_BIN = DIR_SAVE + "data.bin";
    @NotNull public static final String FILE_XML = DIR_SAVE + "jaxb.xml";
    @NotNull public static final String FILE_JSON = DIR_SAVE + "jaxb.json";
    @NotNull public static final String FILE_FASTER_XML= DIR_SAVE + "faster.xml";
    @NotNull public static final String FILE_FASTER_JSON = DIR_SAVE + "faster.json";

}
