package ru.vlasova.iteco.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IRepository;
import ru.vlasova.iteco.taskmanager.api.service.IService;
import ru.vlasova.iteco.taskmanager.entity.AbstractEntity;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    public abstract IRepository<E> getRepository();

    @Override
    public void merge(@Nullable final E entity) {
        if (entity == null) return;
        getRepository().merge(entity);
    }

    @Override
    @Nullable
    public E persist(@Nullable final E entity) throws DuplicateException {
        if (entity == null) return null;
        getRepository().persist(entity);
        return entity;
    }

    @Override
    @Nullable
    public List<E> findAll() {
        return getRepository().findAll();
    }

    @Override
    @Nullable
    public List<E> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        return getRepository().findAll(userId);
    }

    @Override
    @Nullable
    public E findOne(@Nullable final String id) {
        if (id == null) return null;
        return getRepository().findOne(id);
    }

    @Override
    @Nullable
    public E findOneByUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        return getRepository().findOneByUserId(userId, id);
    }

    @Override
    public void remove(@Nullable final String id) {
        if(id == null || id.trim().length() == 0) return;
        getRepository().remove(id);
    }

    @Override
    public void removeAll() {
        getRepository().removeAll();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        getRepository().removeAll(userId);
    }

}
