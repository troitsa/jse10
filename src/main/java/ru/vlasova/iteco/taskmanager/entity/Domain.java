package ru.vlasova.iteco.taskmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {

    @Nullable
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    private List<Project> projects;

    @Nullable
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    private List<Task> tasks;

    @Nullable
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    private List<User> users;

}
