package ru.vlasova.iteco.taskmanager.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IDomainService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.constant.DataConstant;
import ru.vlasova.iteco.taskmanager.entity.Domain;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.File;
import java.io.IOException;

public class FasterJsonLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] getRole() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_faster_json_load";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load with fasterxml from json";
    }

    @Override
    public void execute() throws DuplicateException, IOException {
        TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        if (userId == null) {
            terminalService.print("You are not authorized");
            return;
        }
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        @NotNull final File file = new File(DataConstant.FILE_FASTER_JSON);
        file.getParentFile().mkdirs();

        ObjectMapper mapper = new ObjectMapper ();
        Domain domain = mapper.readValue(file, Domain.class);

        domainService.load(domain);
        terminalService.print("Data has been loaded. Please, login");
    }
}
