package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

public final class TaskClearCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @NotNull
    public Role[] getRole() {
        return new Role[]{Role.USER};
    }

    @Override
    @NotNull
    public String getName() {
        return "task_clear";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        if (userId == null) return;
        taskService.removeAll(userId);
        terminalService.print("All tasks deleted.");
    }

}