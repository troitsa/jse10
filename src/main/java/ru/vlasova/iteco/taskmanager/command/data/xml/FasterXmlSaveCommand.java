package ru.vlasova.iteco.taskmanager.command.data.xml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IDomainService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.constant.DataConstant;
import ru.vlasova.iteco.taskmanager.entity.Domain;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.File;
import java.io.IOException;

public class FasterXmlSaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] getRole() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_faster_xml_save";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save with fasterxml to xml";
    }

    @Override
    public void execute() throws IOException {

        TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();

        if (userId == null) {
            terminalService.print("You are not authorized");
            return;
        }

        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.save(domain);

        @NotNull final File file = new File(DataConstant.FILE_FASTER_XML);
        file.getParentFile().mkdirs();

        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);

        terminalService.print("Data has been saved to xml");

    }

}
