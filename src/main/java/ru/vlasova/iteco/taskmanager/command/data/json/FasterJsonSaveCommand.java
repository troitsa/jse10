package ru.vlasova.iteco.taskmanager.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IDomainService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.constant.DataConstant;
import ru.vlasova.iteco.taskmanager.entity.Domain;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.File;
import java.io.IOException;

public class FasterJsonSaveCommand  extends AbstractCommand {

    @Nullable
    @Override
    public Role[] getRole() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_faster_json_save";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save with fasterxml to json data";
    }

    @Override
    public void execute() throws IOException {

        TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();

        if (userId == null) {
            terminalService.print("You are not authorized");
            return;
        }

        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.save(domain);

        @NotNull final File file = new File(DataConstant.FILE_FASTER_JSON);
        file.getParentFile().mkdirs();

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);

        terminalService.print("Data has been saved to json");

    }
}
