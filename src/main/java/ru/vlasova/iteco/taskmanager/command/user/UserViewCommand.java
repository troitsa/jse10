package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

public final class UserViewCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @NotNull
    public Role[] getRole() {
        return new Role[] {Role.USER, Role.ADMIN};
    }

    @Override
    @NotNull
    public String getName() {
        return "user_view";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show user data";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final User user = serviceLocator.getCurrentUser();
        if (user == null) {
            terminalService.print("You are not authorized");
        }
        else {
            terminalService.print(user.toString());
        }
    }

}