package ru.vlasova.iteco.taskmanager.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IDomainService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.constant.DataConstant;
import ru.vlasova.iteco.taskmanager.entity.Domain;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class BinaryLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] getRole() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_binary_load";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load binary data";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException, DuplicateException {
        TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        if (userId == null) {
            terminalService.print("You are not authorized");
            return;
        }
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        @NotNull final FileInputStream inputStream = new FileInputStream(DataConstant.FILE_BIN);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @Nullable final Domain domain = (Domain)objectInputStream.readObject();
        domainService.load(domain);
        objectInputStream.close();
        terminalService.print("Data has been loaded. Please, login");
    }

}
