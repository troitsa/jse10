package ru.vlasova.iteco.taskmanager.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IDomainService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.constant.DataConstant;
import ru.vlasova.iteco.taskmanager.entity.Domain;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;

public class JaxBXmlSaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] getRole() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_jaxb_xml_save";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save with jaxb to xml";
    }

    @Override
    public void execute() throws JAXBException, IOException {
        TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        if (userId == null) {
            terminalService.print("You are not authorized");
            return;
        }
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.save(domain);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final File file = new File(DataConstant.FILE_XML);
        file.getParentFile().mkdirs();
        @NotNull final FileOutputStream outputStream = new FileOutputStream(file);
        marshaller.marshal(domain, file);
        outputStream.close();
        terminalService.print("Data has been saved to xml");
    }

}