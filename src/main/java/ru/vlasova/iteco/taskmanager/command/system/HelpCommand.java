package ru.vlasova.iteco.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

public class HelpCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    @Nullable
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    @NotNull
    public String getName() {
        return "help";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        for(@NotNull final AbstractCommand command : serviceLocator.getStateService().getCommands()) {
            terminalService.print(command.getName() + ": " + command.getDescription());
        }
    }

}