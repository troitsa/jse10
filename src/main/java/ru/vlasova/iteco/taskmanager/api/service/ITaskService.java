package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    Task insert(@Nullable final String userId, @Nullable final String name,
                @Nullable final String description, @Nullable final String dateStart,
                @Nullable final String dateFinish);

    @Nullable
    Task getTaskByIndex(@Nullable final String userId, int index);

    void removeTasksByProjectId(@Nullable final String userId, @Nullable final String id);

    @Nullable
    List<Task> getTasksByProjectId(@Nullable final String userId, @Nullable final String projectId);

    void remove(@Nullable final String userId, @Nullable final String id);

    void remove(@Nullable final String userId, int index);

    @Nullable
    List<Task> search(@Nullable final String userId, @Nullable final String searchString);

}
