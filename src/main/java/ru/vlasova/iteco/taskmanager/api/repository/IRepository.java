package ru.vlasova.iteco.taskmanager.api.repository;

import ru.vlasova.iteco.taskmanager.entity.AbstractEntity;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull String userId);

    @Nullable
    E findOne(@NotNull String id);

    @Nullable
    E findOneByUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    E persist(@NotNull E obj) throws DuplicateException;

    void merge(@NotNull E obj);

    void remove(@NotNull String id);

    void remove(@NotNull String userId, @NotNull String id);

    void removeAll();

    void removeAll(@NotNull String userId);

    int count();

}
