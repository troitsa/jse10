package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    String getIdByIndex(@NotNull final String userId, final int projectIndex);

    @NotNull
    List<Project> search(@NotNull String userId, @NotNull String searchString);

}
